import WebSocket from "ws";

class User {
    private readonly playerId: string;
    private readonly connection: WebSocket;
    private inputBuffer: string[];
    private outputBuffer: any[];

    constructor(playerId: string, connection: WebSocket) {
        this.playerId = playerId;
        this.connection = connection;
        this.inputBuffer = [];
        this.outputBuffer = [];
    }

    getPlayerId(): string {
        return this.playerId;
    }

    sendHostOutput(data: any): void {
        this.connection.send(JSON.stringify(data));
    }

    handleHostOutput(): any {
        return this.outputBuffer.shift();
    }

    isInputReady(): boolean {
        return this.connection.readyState === WebSocket.OPEN && this.inputBuffer.length > 0;
    }

    isOutputReady(): boolean {
        return this.connection.readyState === WebSocket.OPEN && this.outputBuffer.length > 0
    }

    queueHostInput(data: WebSocket.Data) {
        if (typeof (data) === 'string') {
            this.inputBuffer.push(data);
        }
    }

    queueHostOutput(user: string, data: string): void {
        this.outputBuffer.push({ user: user, message: data });
    }

    handleHostInput(): string {
        return this.inputBuffer.shift();
    }

    ping(): void {
        this.connection.ping();
    }
}

export { User, WebSocket }
