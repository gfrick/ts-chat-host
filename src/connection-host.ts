import WebSocket, { Data } from 'ws';
import { User } from "./user";
import Logger from './logger';

export default class ConnectionHost {
    public users: User[];
    public serverOutput = [];

    constructor() {
        this.users = [];
    }

    public handleConnection(ws: WebSocket, wss: WebSocket.Server, playerId: string): void {
        const newUser = new User(playerId, ws);
        newUser.queueHostOutput("Host", 'Welcome to the chat room!');
        this.serverEcho("New connection from " + playerId);

        if (this.users.findIndex(existing => existing.getPlayerId() === playerId) !== -1) {
            newUser.queueHostOutput("Host", "Already playing");
            ws.close(4403, "already_playing");
            return;
        }
        ws.on('message', (data: Data) => {
            newUser.queueHostInput(data);
        });
        ws.on('close', (data: Data) => {
            Logger.info("Connection closed");
            this.users.splice(this.users.findIndex(existing => existing.getPlayerId() === playerId), 1);
            this.serverEcho("Connection closed for " + playerId);
        });
        ws.on('pong', (data: Data) => {
            Logger.info("*** PONG ***");
        });
        this.users.push(newUser);
        return;
    }

    public hostTick(): void {
        this.pumpInput()
        this.pumpOutput();
    }

    public pumpInput(): void {
        this.users.forEach((inputUser) => {
            let inputUserId = inputUser.getPlayerId();
            if (!inputUser.isInputReady()) return;
            const input = inputUser.handleHostInput();
            if (!input) return;
            this.users.forEach((outputUser) => {
                if( outputUser.getPlayerId() === inputUser.getPlayerId() ) return;
                outputUser.queueHostOutput(inputUserId, input);
            });
        });
    }

    public pumpOutput(): void {
        this.users.forEach((outputUser) => {
            while (outputUser.isOutputReady()) {
                const output = outputUser.handleHostOutput();
                if (!output) {
                    return;
                }
                outputUser.sendHostOutput(output);
            }
        });
    }

    public serverEcho( message: string): void {
        this.users.forEach((user) => {
            user.queueHostOutput("Host", message);
        });
    }
}

