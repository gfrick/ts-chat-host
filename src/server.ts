import express, { Application } from 'express';
import http, { IncomingMessage } from 'http';
import WebSocket from 'ws';
import ConnectionHost from "./connection-host";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import Logger from "./logger";
import { Socket } from "net";

const application: Application = express();
application.use(bodyParser.urlencoded({ extended: true }));
application.use(bodyParser.json());
application.use(cookieParser());
application.use('/public', express.static(__dirname + '/public'));

const connectionHost = new ConnectionHost();
Logger.info("Starting Server.");
startServer(connectionHost);

function startServer(connectionHost: ConnectionHost) {
    const PORT = process.env.PORT || 8080;
    const server = http.createServer(express);
    const wss = new WebSocket.Server({ noServer: true })

    server.on('request', application);
    server.on('upgrade', function upgrade(request, socket, head) {
        Logger.info("Upgrade request received.");

        if (request.url.indexOf("=") < 0 || request.url.indexOf("?key=") < 0) {
            socket.write('HTTP/1.1 400 Bad Request\r\n\r\n');
            socket.destroy();
            return;
        }
        const username = request.url.split("=")[1];
        Logger.info(`Upgrade request for ${username}.`);

        wss.handleUpgrade(request, socket as Socket , head, function done(ws) {
            wss.emit('connection', ws, request, username);
        });
        Logger.info("Upgrade request handled.");
    });

    wss.on('connection', function connection(ws: WebSocket, request: IncomingMessage, username: string) {
        Logger.info(`With account ${username}.`);
        connectionHost.handleConnection(ws, wss, username);
    })

    setInterval(() => {
        connectionHost.hostTick();
    }, 500);

    server.listen(PORT, (): void => {
        Logger.info(`Server is listening on ${PORT}!`);
    });
}
